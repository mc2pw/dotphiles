# Add `~/.bin` to the `$PATH`
export PATH="$HOME/.bin:$PATH";

# Based on https://unix.stackexchange.com/questions/6463/find-searching-in-parent-directories-instead-of-subdirectories
function parentfind() {
  path="$(pwd)"
  while [[ $path != / ]];
  do
    out="$(find "$path" -maxdepth 1 -mindepth 1 "$@")"
    [ -z "$out" ] || {
      echo "$out"
      break
    }
    # Note: if you want to take symlinks into account, use "$(readlink -f "$path"/..)"
    path="$(realpath -s "$path"/..)"
  done
}

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.{bash_path,bash_prompt,bash_exports,bash_aliases,bash_functions,bash_extra}; do
    [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# append to the history file, don't overwrite it
shopt -s histappend

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Enable tab completion for `g` by marking it as an alias for `git`
_completion_loader git
type _git &> /dev/null && complete -o bashdefault -o default -o nospace -F _git g

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;

# Add completion for todo.txt
[ -f ~/.bin/todo_completion ] && . ~/.bin/todo_completion
type _todo &> /dev/null && complete -o bashdefault -o default -o nospace -F _todo t

# Add completion for tmux
[ -f ~/.bin/tmux_completion ] && . ~/.bin/tmux_completion
if type _tmux_complete_session &> /dev/null; then
    function _tat() {
        _tmux_complete_session "${COMP_WORDS[COMP_CWORD]}"
    }
    complete -o nospace -F _tat tat
fi

# Enable rupa/z
[ -f ~/.bin/z.sh ] && . ~/.bin/z.sh

# Config for the current todo project
TODO_BASHRC="$(todo-root)/bashrc"
[ -f "$TODO_BASHRC" ] && . "$TODO_BASHRC"
